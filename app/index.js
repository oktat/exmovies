const express = require('express')
const app = express()
const PORT=8000
const router = require('./routes/api')


app.use('/api', router)

app.listen(PORT, () => {
    console.log(`The server listening on port: ${PORT}`)
})
