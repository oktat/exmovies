const Router = require('express')
const MovieController = require('../controllers/movieController')
const router = Router()

router.get('/movies', MovieController.index)

module.exports = router
