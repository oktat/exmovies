const { DataType } = require('sequelize')
const sequelize = require('../database/database')

const Movie = sequelize.define('movie', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    title: { type: DataType.STRING, allowNull: false },
    length: { type: DataType.INTEGER, allowNull: true}
})